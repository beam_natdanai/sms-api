const socket = io();

socket.on('message', (message) => {
    // test message from ranking
    console.log(message);
});

socket.on('smsSent', (sms) => {
    // do something
    // push on table
    document.querySelector('#message').append('\n' + sms.sms_text);
});

document.querySelector('#complete-btn').addEventListener('click', (e) => {
    
    const transaction = {
        member_id: "5ce434c2f6ba132f20de7646",
        result: 20,
        investment: 500,
        total: 30,
        mode: 'hard',
        time_start: 15589448809763,
        time_end: 15589448809763,
    };

    socket.emit('playComplete', transaction, (message) => {
        // callback message if transact was sent!
        console.log(message);
    });
});

// document.querySelector('#location-btn').addEventListener('click', (ev) => {
//     if(!navigator.geolocation) {
//         return alert('Geolocation not support on browser');
//     }

//     navigator.geolocation.getCurrentPosition((position) => {
//         socket.emit('sendLocation', {
//             latitude: position.coords.latitude,
//             longitude: position.coords.longitude,
//         });
//     });
// });