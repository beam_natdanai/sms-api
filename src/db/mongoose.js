const username = 'sms-project';
const password = encodeURIComponent('y2021smsSystem'); 

const mongoose = require('mongoose');
const connect = 'mongodb://' + username + ':' + password + '@127.0.0.1:27017/sms';
// const connect = 'mongodb://' + username + ':' + password + '@34.80.146.37:27017/smsdb';
// const connect = 'mongodb://sms_user:mysmspassword@206.189.151.224/smsdb';
// const connect = 'mongodb://localhost:27017/smsdb';
const Sentry = require('../sentry/sentryio');
try {
    mongoose.connect(connect, {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        useCreateIndex: true,
        useFindAndModify: false,
        poolSize: 10
    });
} catch (error) {
    Sentry.captureException(error);
    console.log(error);
}
