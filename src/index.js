// Require
const path = require('path');
const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const cors = require('cors');
const socketio = require('socket.io');
require('./db/mongoose');

// Routers
const smsRouter = require('./routers/sms');
const deviceRouter = require('./routers/device');

// Init Call
const app = express();
const server = http.createServer(app);
const io = socketio(server, { pingTimeout: 10000, upgradeTimeout: 20000, destroyUpgradeTimeout: 3000 });
app.set('socketio', io);

const port = process.env.port || 5000
// const publicDirectoryPath = path.join(__dirname, '../public');

// Use
app.use(cors());
// app.use(express.static(publicDirectoryPath));
app.use(bodyParser.json({limit: '200mb'}));
app.use(bodyParser.urlencoded({limit: '200mb', extended: true}));
app.use(express.json());
app.use(smsRouter);
app.use(deviceRouter);

// real time
io.on('connection', (socket) => {
    
    // server recieve sms from client
    socket.on('smsSent', (sms, callback) => {
        sms.forEach(element => {
            io.emit('smsSent', element);
        });
        callback('sms sent!');
    });

    // disconnect server 
    socket.on('disconnect', () => {
        // window.clearInterval(intervalRanking);
    });

});

// Listen
server.listen(port, () => {
    console.log('Server is up on port ' + port);
});