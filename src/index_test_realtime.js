const path = require('path');
const http = require('http');
const express = require('express');
const socketio = require('socket.io');

// create express
const app = express();

// create new server to run app
const server = http.createServer(app);

// create web socket
const io = socketio(server);

const port = process.env.PORT || 3000
const publicDirectoryPath = path.join(__dirname, '../public');

app.use(express.static(publicDirectoryPath));


io.on('connection', (socket) => {
    console.log('New WebSocket connection');
    
    socket.emit('message', 'Welcome!');
    socket.broadcast.emit('message', 'A new user has joined!');

    socket.on('disconnect', () => {
        io.emit('message', 'a user has left');
    });

    socket.on('sendLocation', (position) => {
        io.emit('message', position);
    });
});

server.listen(port, () => {
    console.log('Server is up on port', port);
});