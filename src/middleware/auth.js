const jwt = require('jsonwebtoken');
const Member = require('../models/member');

const auth = async (req, res, next) => {
    try {
        const token = req.header('Authorization');
        const decoded = jwt.verify(token, 'AuthenticateToken');
        const member = await Member.findOne({ _id: decoded._id, 'tokens.token': token });

        if(!member) {
            throw new Error();
        }

        req.token = token;
        req.member = member;
        next();
        
    } catch (error) {
        res.status(401).send({ error: 'Please Login First' });
    }
};

module.exports = auth;