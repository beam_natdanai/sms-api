const mongoose = require('mongoose');

const DeviceSchema = mongoose.Schema({
    device_id: {
        type: String
    },
    device_name: {
        type: String
    },
    telno: {
        type: String
    },
    bank: {
        type: String
    },
    name: {
        type: String
    }
}, {
    timestamps: true
});

const DeviceModel = mongoose.model('device', DeviceSchema);

module.exports = DeviceModel;