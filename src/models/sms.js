const mongoose = require('mongoose');

const SmsSchema = mongoose.Schema({
    sms_address: {
        type: String
    },
    sms_text: {
        type: String
    },
    date: {
        type: String
    },
    time: {
        type: String
    },
    account: {
        type: String
    },
    type: {
        type: String
    },
    account_from: {
        type: String
    },
    amount: {
        type: Number
    },
    sms_datetime: {
        type: Date
    },
    transaction_id: {
        type: Number
    },
    device_id: {
        type: String
    },
    device_name: {
        type: String
    },
    telno: {
        type: String
    }
}, {
    timestamps: true
});

const SmsModel = mongoose.model('sms', SmsSchema);

module.exports = SmsModel;