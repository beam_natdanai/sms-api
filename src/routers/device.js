const express = require('express');
const Device = require('../models/device');
const moment = require('moment');
const router = new express.Router();
const Sentry = require('../sentry/sentryio');

router.post('/device', async (req, res) => {
    const newDevice = req.body;

    try {
        const result = await Device.insertMany(newDevice);
        res.status(201).send({ device: result });
    } catch (error) {
        console.log(error);
        Sentry.captureException(error);
        res.status(400).send(error);
    }

});

router.get('/device', async (req, res) => {
    try {
        const result = await Device.find({});
        res.status(200).send({ device: result });
    } catch (error) {
        console.log(error);
        Sentry.captureException(error);
        res.status(400).send(error);
    }

});

router.patch('/device/:id', async (req, res) => {
    const _id = req.params.id;
    const data = req.body;

    const updates = Object.keys(data);
    const allowUpdates = ["device_name", "telno", "bank", "name"];
    const isValidOperation = updates.every((update) => allowUpdates.includes(update));

    if(!isValidOperation) {
        return res.status(400).send({ error: 'Invalid Updates' });
    }

    try {
        // const member = await Member.findByIdAndUpdate(_id, data, { new: true, runValidators: true });

        const device = await Device.findById(_id);

        updates.forEach((update) => {
            device[update] = data[update]
            if (!device.hasOwnProperty(update)) {
                device[update] = data[update];
            }
        });

        await device.save();

        if(!device) {
            return res.status(404).send();
        }

        res.send(device);

    } catch (error) {
        res.status(400).send(error);
    }
});

module.exports = router;