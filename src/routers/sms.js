const express = require('express');
const Sms = require('../models/sms');
const moment = require('moment');
const router = new express.Router();
const Sentry = require('../sentry/sentryio');

router.get("/", async (req, res) => {
    res.send("Welcome");
});

router.post('/sms', async (req, res) => {
    let io = req.app.get('socketio');
    const newSms = req.body.data;
    console.log(req.body);

    try {

        const result = await Sms.insertMany(newSms);

        io.emit('smsSent', result);

        res.status(201).send({ sms: result });
    } catch (error) {
        console.log(error);
        Sentry.captureException(error);
        res.status(400).send(error);

    }

});

router.get('/sms/today', async (req, res) => {
    const type = req.query.type;
    const date = req.query.date;
    const device_id = req.query.device_id;
    const device_name = req.query.device_name;
    const sms_address = req.query.sms_address;

    const param = {
        type: type,
        date: date,
        device_id: device_id,
        device_name: device_name,
        sms_address: sms_address
    };

    if(!device_id) delete param.device_id;
    if(!device_name) delete param.device_name;
    if(!sms_address) delete param.sms_address;

    try {
        const sms = await Sms.find(param).sort({ createdAt: -1 });

        const totalSms = await Sms.aggregate([
            {
                $match: param,
            },
            {
                $group: {
                    _id: 'total',
                    totalTransaction: { $sum: "$amount" },
                    transaction: { $sum: 1 }
                }

            }
        ]);

        const totalTransaction = numberWithCommas(totalSms[0].totalTransaction);
        const transaction = numberWithCommas(totalSms[0].transaction);

        const result = {
            totalSms: {
                totalTransaction: "ยอดเงินวันนี้ " + (totalTransaction || 0) + " บาท",
                transaction: "ออเดอร์ " + (transaction || 0) + " รายการ"
            },
            sms: sms
        };

        res.status(200).send(result);
    } catch (error) {
        Sentry.captureException(error);
        res.status(400).send(error);
    }
});

router.get('/sms/yesterday', async (req, res) => {
    const type = req.query.type;
    const date = req.query.date;
    const device_id = req.query.device_id;
    const device_name = req.query.device_name;
    const sms_address = req.query.sms_address;

    const param = {
        type: type,
        date: date,
        device_id: device_id,
        device_name: device_name,
        sms_address: sms_address
    };

    if(!device_id) delete param.device_id;
    if(!device_name) delete param.device_name;
    if(!sms_address) delete param.sms_address;

    try {
        const sms = await Sms.find(param).sort({ createdAt: -1 });

        const totalSms = await Sms.aggregate([
            {
                $match: param,
            },
            {
                $group: {
                    _id: 'total',
                    totalTransaction: { $sum: "$amount" },
                    transaction: { $sum: 1 }
                }

            }
        ]);

        const totalTransaction = numberWithCommas(totalSms[0].totalTransaction);
        const transaction = numberWithCommas(totalSms[0].transaction);

        const result = {
            totalSms: {
                totalTransaction: "ยอดเงินเมื่อวาน " + (totalTransaction || 0) + " บาท",
                transaction: "ออเดอร์ " + (transaction || 0) + " รายการ"
            },
            sms: sms
        };

        res.status(200).send(result);
    } catch (error) {
        Sentry.captureException(error);
        res.status(400).send(error);
    }
});

router.get('/sms/between', async (req, res) => {
    const type = req.query.type;
    const device_id = req.query.device_id;
    const device_name = req.query.device_name;
    const sms_address = req.query.sms_address;
    let start_date = req.query.startDate;
    let end_date = req.query.endDate;

    if (start_date == "") {
        start_date = end_date
    } 
    else if (end_date == "") {
        end_date = start_date
    }

    start_date = moment(start_date).toISOString();
    end_date = moment(end_date).add(1, 'days').toISOString();

    const param = {
        type: type,
        device_id: device_id,
        device_name: device_name,
        sms_address: sms_address,
        createdAt: {
            $gt: start_date,
            $lte: end_date
        }
    };

    if(!device_id) delete param.device_id;
    if(!device_name) delete param.device_name;
    if(!sms_address) delete param.sms_address;

    try {
        const sms = await Sms.find(param).sort({ createdAt: -1 });

        param.createdAt.$gt = new Date(start_date);
        param.createdAt.$lte = new Date(end_date);

        const totalSms = await Sms.aggregate([
            {
                $match: param,
            },
            {
                $group: {
                    _id: 'total',
                    totalTransaction: { $sum: "$amount" },
                    transaction: { $sum: 1 }
                }

            }
        ]);

        const totalTransaction = numberWithCommas(totalSms[0].totalTransaction);
        const transaction = numberWithCommas(totalSms[0].transaction);

        const result = {
            totalSms: {
                totalTransaction: "ยอดเงิน " + (totalTransaction || 0) + " บาท",
                transaction: "ออเดอร์ " + (transaction || 0) + " รายการ"
            },
            sms: sms
        };

        res.status(200).send(result);
    } catch (error) {
        Sentry.captureException(error);
        res.status(400).send(error);
    }
});

numberWithCommas = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

module.exports = router;