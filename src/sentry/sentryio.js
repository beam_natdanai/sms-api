const Sentry = require('@sentry/node');

Sentry.init({ dsn: 'https://7408f73b86b2416b98d12460e1ba8be1@o406424.ingest.sentry.io/5308081' });

module.exports = Sentry;